// ** React Imports
import { useState, Fragment, useRef, useEffect } from 'react'

// ** Reactstrap Imports
import { Card, CardHeader, CardTitle, CardBody, Button, ListGroup, ListGroupItem, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { FaCropSimple } from "react-icons/fa6";
// ** Third Party Imports
import toast from 'react-hot-toast'
import { useDropzone } from 'react-dropzone'
import { X, DownloadCloud, Edit } from 'react-feather'
import { canvasPreview } from './canvasPreview'
import ReactCrop, { centerCrop, makeAspectCrop } from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css';


const Crop = ({ children, name, setFieldValue, values }) => {
    // ** State
    const [files, setFiles] = useState([])
    const [imgSrc, setImgSrc] = useState(null);
    const previewCanvasRef = useRef(null);
    const imgRef = useRef(null);
    const [scale, setScale] = useState(1);
    const [rotate, setRotate] = useState(0);
    const [aspect, setAspect] = useState(16 / 9);
    const [crop, setCrop] = useState(centerCrop(16, 9)); // Initialize with a default aspect ratio (16:9)
    const [completedCrop, setCompletedCrop] = useState(null);
    const [modalOpen, setModalOpen] = useState(false);
    const [editImage, setEditImag] = useState("");


    const { getRootProps, getInputProps } = useDropzone({
        multiple: true,
        accept: {
            'image/*': ['.png', '.jpg', '.jpeg', '.gif']
        },
       
               onDrop: (acceptedFiles, rejectedFiles) => {
  if (rejectedFiles.length) {
    toast.error('You can only upload image Files!.');
  } else {
    // Use FileReader to read each file as base64 and store them in an array
    const base64Array = [];

    acceptedFiles.forEach(file => {
      const reader = new FileReader();

      reader.onload = function () {
        base64Array.push({name:file?.name, base64URL:reader.result});

        // Check if all files have been processed
        if (base64Array.length === acceptedFiles.length) {
          // Do something with the array of base64 strings
          console.log('All files as base64:', base64Array);
           setFiles([...files, ...base64Array])
                setFieldValue(name, [...files, ...base64Array]);
        }
      };

      reader.readAsDataURL(file);
    });

    // Set state or handle base64Array as needed
    // setFiles([...files, ...acceptedFiles.map(file => Object.assign(file))]);
    // setFieldValue(name, acceptedFiles);
  }
}})


useEffect(()=>{

    if(values?.image === null){
        setFiles([])
    }
},[values])

const handleSubEmp = ()=>{
    setFiles([])
}

    const handleRemoveFile = file => {
        const uploadedFiles = files
        const filtered = uploadedFiles.filter(i => i.name !== file.name)
        setFiles([...filtered])
        setImgSrc(null);
    }

    const handleCrop = (names, filesObj) => {
        setModalOpen(true)
        setEditImag(names)
        setImgSrc(filesObj?.base64URL ? filesObj?.base64URL: URL.createObjectURL(filesObj));

    }

    const renderFileSize = size => {
        if (Math.round(size / 100) / 10 > 1000) {
            return `${(Math.round(size / 100) / 10000).toFixed(1)} mb`
        } else {
            return `${(Math.round(size / 100) / 10).toFixed(1)} kb`
        }
    }


    const fileList = files?.map((file, index) => (
        <ListGroupItem key={`${file.name}-${index}`} className='d-flex align-items-center justify-content-between'>

            <div className='file-details d-flex align-items-center'>
                <div className='file-preview me-1'>
                    <img className='rounded' alt={file.name} src={file?.base64URL ? file?.base64URL :URL.createObjectURL(file)} height='28' width='28' />
                </div>
                <div>
                    <p className='file-name mb-0'>{file.name}</p>
                    {/* <p className='file-size mb-0'>{renderFileSize(file.size)}</p> */}
                </div>
            </div>
           <div>
             <Button color='danger' outline size='sm' className='btn-icon me-1' onClick={() => handleCrop(file.name, file)}>
                <FaCropSimple size={14} />
            </Button>
            <Button color='danger' outline size='sm' className='btn-icon' onClick={() => handleRemoveFile(file)}>
                <X size={14} />
            </Button>
           </div>
        </ListGroupItem>
    ))




    const handleRemoveAllFiles = () => {
        setFiles([])
    }

    function centerAspectCrop(mediaWidth, mediaHeight, aspect) {
        return centerCrop(
            makeAspectCrop(
                {
                    unit: "%",
                    width: 90,
                },
                aspect,
                mediaWidth,
                mediaHeight
            ),
            mediaWidth,
            mediaHeight
        );
    }

    const handleCropChange = (xyz, crops) => {
        setCrop(crops);
        setCompletedCrop(xyz);
        if (
            completedCrop?.width &&
            completedCrop?.height &&
            imgRef.current &&
            previewCanvasRef.current
        ) {
            // We use canvasPreview as it's much faster than imgPreview.
            canvasPreview(
                imgRef.current,
                previewCanvasRef.current,
                xyz,
                1, // scale
                0 // rotate
            );
        }
    };

    const handleModalClose = () => {
        setModalOpen(false);
    };

    function onImageLoad(e) {
        if (aspect) {
            const { width, height } = e.currentTarget;
            setCrop(centerAspectCrop(width, height, aspect));
        }
    }

    const downloadImage = () => {
        const canvas = previewCanvasRef?.current;
        if (!canvas) {
            console.error('Canvas not found');
            return;
        }

        const newarray = values?.image;
        const base64Array = [];
console.log(newarray, "asasas")
        newarray.forEach((item) => {


            if (item?.name === editImage) {
                base64Array.push({base64URL:canvas.toDataURL() , name: editImage})
            } else {

                base64Array.push(item)

            }
        })

        setFieldValue(name, base64Array);
        setFiles(base64Array)

        // Log the cropped image data to the console
        const formData = new FormData()
        formData.append("first_image", canvas.toDataURL())
        // Close the modal
        handleModalClose();
    };

    return (
        <>

            <div {...getRootProps({ className: 'dropzone' })} style={{background: "#e6e6e6", margin: "10px", padding: "10px"}}>
                <input {...getInputProps()} />
                <div className='d-flex align-items-center justify-content-center flex-column'>
                    <DownloadCloud size={64} />
                    <h5>Drop Files here or click to upload</h5>
                    <p className='text-secondary'>
                        Drop files here or click{' '}
                        <a href='/' onClick={e => e.preventDefault()}>
                            browse
                        </a>{' '}
                        thorough your machine
                    </p>
                </div>
            </div>
            {files.length ? (
                <Fragment>
                    <Modal size='xl' isOpen={modalOpen} toggle={handleModalClose}>
                        <ModalHeader toggle={handleModalClose}>Crop Image</ModalHeader>
                        <ModalBody className='d-flex'>
                            {imgSrc && (
                                <ReactCrop
                                    crop={crop}
                                    onChange={(_, percentCrop) => handleCropChange(_, percentCrop)}
                                    onComplete={(c) => setCompletedCrop(c)}
                                    aspect={false}
                                    minWidth={50}
                                    minHeight={50}

                                    style={{
                                        width: "50%",
                                        height: "500px",
                                        marginBottom: 50,
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                    }}
                                    className='me-2'
                                >
                                    <img
                                        ref={imgRef}
                                        className="fit-cover"
                                        alt="Crop me"
                                        src={imgSrc}
                                        style={{
                                            transform: `scale(${scale}) rotate(${rotate}deg)`,
                                            width: "100%",
                                            height: "100%",
                                            objectFit: "contain",
                                        }}
                                        onLoad={onImageLoad}
                                    />
                                </ReactCrop>
                            )}
                            {!!completedCrop && (
                                <div
                                    style={{
                                        width: "40%",
                                        height: "500px",
                                        marginBottom: 50,
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        background: "lgray",
                                    }}
                                >
                                    <canvas
                                        ref={previewCanvasRef}
                                        style={{
                                            objectFit: "contain",
                                            width: "100%",
                                            height: "100%",
                                        }}
                                    />
                                </div>
                            )}
                        </ModalBody>
                        <ModalFooter>
                            <Button color='danger' onClick={handleModalClose}>
                                Close
                            </Button>
                            <Button color='primary' onClick={() => downloadImage()}>
                                Save image
                            </Button>
                        </ModalFooter>
                    </Modal>
                    <ListGroup className='my-2'>{fileList}</ListGroup>
                    <div className='d-flex justify-content-end'>
                        <Button className='me-1' color='danger' outline onClick={handleRemoveAllFiles}>
                            Remove All
                        </Button>
                       
                    </div>
                </Fragment>
            ) : null}
        </>
    )
}

export default Crop
