import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Layout from '../layouts/Layout'
import BCard from "../Components/BCards"
import AddCard from '../Components/AddCard/AddCard'
import Crop from '../Components/AddCard/Crop'
const Router = () => {
    return (
            <BrowserRouter>
            <Routes>
                <Route element={<Layout />} path='/'>
                    <Route element={<BCard />}  index />
                    <Route element={<AddCard />}  path='/add/card' />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default Router